
## Instruction

```java
package ir.bardiademon.GetIMEI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    private IMEI imei;

    private TextView txtIMEI;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);

        txtIMEI = findViewById (R.id.txt_imei);

        imei = new IMEI (this , (IsPermission , IMEI) ->
        {
            if (IsPermission) txtIMEI.setText (IMEI);
        });
        imei.getIMEI ();

    }

    @Override
    public void onRequestPermissionsResult (int requestCode , @NonNull String[] permissions , @NonNull int[] grantResults)
    {
        imei.onRequestPermissionsResult (requestCode , grantResults);
    }
}


```