package ir.bardiademon.GetIMEI;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class IMEI
{
    private String imei = "";

    private static int CODE_REQUEST_PERMISSION = 0;

    private Activity activity;

    private boolean requestPermission;

    private AfterGet afterGet;

    public IMEI (Activity _Activity)
    {
        this (_Activity , null);
    }

    public IMEI (Activity _Activity , AfterGet _AfterGet)
    {
        this (_Activity , _AfterGet , true);
    }

    public IMEI (Activity _Activity , AfterGet _AfterGet , boolean RequestPermission)
    {
        this.activity = _Activity;
        this.afterGet = _AfterGet;
        this.requestPermission = RequestPermission;
        checkPermission ();
    }

    private void checkPermission ()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (isPermission ()) getIMEI ();
            else
            {
                if (requestPermission)
                    ActivityCompat.requestPermissions (activity , new String[]{Manifest.permission.READ_PHONE_STATE} , CODE_REQUEST_PERMISSION);
            }
        }
        else getIMEI ();
    }

    public void getIMEI ()
    {
        get ();
    }

    @SuppressLint ({"MissingPermission" , "HardwareIds"})
    private void get ()
    {
        if (isPermission ())
        {
            TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService (Context.TELEPHONY_SERVICE);
            if (telephonyManager != null)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    imei = telephonyManager.getImei ();
                else imei = telephonyManager.getDeviceId ();
            }
        }
        if (afterGet != null) afterGet.After (isPermission () , imei);
    }

    public String getImei ()
    {
        return imei;
    }

    public boolean isPermission ()
    {
        return (ContextCompat.checkSelfPermission (activity , Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED);
    }

    public void onRequestPermissionsResult (int requestCode , @NonNull int[] grantResults)
    {
        if (requestCode == IMEI.CODE_REQUEST_PERMISSION)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                get ();
        }
    }

    public interface AfterGet
    {
        void After (boolean IsPermission , String IMEI);
    }
}
